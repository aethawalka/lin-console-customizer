# lin-console-customize

Here is some useful info and code for customize the console.

![](http://i.gyazo.com/608abe460a49ecd1e1cee81c9cd5469f.png)

* [Colors](http://www.arwin.net/tech/bash.php);
* [Show Git branch](http://www.leaseweblabs.com/2013/08/git-tip-show-your-branch-name-on-the-linux-prompt/), [Masterpiece Git branch](http://stackoverflow.com/a/24716445);
